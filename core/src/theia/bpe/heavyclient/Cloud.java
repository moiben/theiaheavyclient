package theia.bpe.heavyclient;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Cloud {
	private BlockingQueue<Vertex[]> queue;

	public Cloud() {
		queue = new ArrayBlockingQueue<>(20000);
	}

	public BlockingQueue<Vertex[]> getQueue() {
		return queue;
	}

}
