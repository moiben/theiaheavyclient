package theia.bpe.heavyclient;

import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Service qui r�cup�re les donn�es d'un socket UDP, passe ces donn�es � un
 * Handler et transmet celui-ci � un ExecutorService pour qu'il soit execut�.
 * 
 * @author Benjamin Peyresaubes
 *
 */
public abstract class ReceivingService implements Runnable {
	protected final DatagramSocket dgSocket;
	protected final int packetSize;

	public ReceivingService(int port, int packetSize) throws SocketException {
		this.dgSocket = new DatagramSocket(port);
		this.packetSize = packetSize;
	}
}
