package theia.bpe.heavyclient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class SkeletonReceivingService extends ReceivingService {
	private Body skeletonToRender;

	public SkeletonReceivingService(int port, int packetSize, Body skeletonToRender) throws SocketException {
		super(port, packetSize);
		this.skeletonToRender = skeletonToRender;
	}

	@Override
	public void run() {
		byte[] buffer = new byte[packetSize];

		while (true) {
			DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
			try {
				dgSocket.receive(receivePacket);

				byte[] data = receivePacket.getData();
				int actualLength = receivePacket.getLength();
				Vertex[] newVertex = new Vertex[25];

				int vertexIndex = 0;
				int dataIndex = 9; // 0 to 7 = timestamp, 8 = tag
				while (dataIndex < data.length && dataIndex < actualLength) {

					float x = ByteBuffer.wrap(Arrays.copyOfRange(data, dataIndex, dataIndex + 4))
							.order(ByteOrder.LITTLE_ENDIAN).getFloat();
					float y = ByteBuffer.wrap(Arrays.copyOfRange(data, dataIndex + 4, dataIndex + 8))
							.order(ByteOrder.LITTLE_ENDIAN).getFloat();
					float z = ByteBuffer.wrap(Arrays.copyOfRange(data, dataIndex + 8, dataIndex + 12))
							.order(ByteOrder.LITTLE_ENDIAN).getFloat();

					byte r = data[dataIndex + 12];
					byte g = data[dataIndex + 13];
					byte b = data[dataIndex + 14];

					newVertex[vertexIndex] = new Vertex(x, y, z, r, g, b, (byte) 0);
					vertexIndex++;
					dataIndex += 16;
				}

				skeletonToRender.setJoints(newVertex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
