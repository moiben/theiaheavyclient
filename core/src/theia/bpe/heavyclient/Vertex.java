package theia.bpe.heavyclient;

/**
 * Encapsule les donn�es d'un point.
 * 
 * @author Benjamin Peyresaubes
 *
 */
public class Vertex {
	private float x;
	private float y;
	private float z;
	private byte r;
	private byte g;
	private byte b;
	private byte tag;

	public Vertex(float x, float y, float z, byte r, byte g, byte b, byte tag) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.r = r;
		this.g = g;
		this.b = b;
		this.tag = tag;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public byte getR() {
		return r;
	}

	public void setR(byte r) {
		this.r = r;
	}

	public byte getG() {
		return g;
	}

	public void setG(byte g) {
		this.g = g;
	}

	public byte getB() {
		return b;
	}

	public void setB(byte b) {
		this.b = b;
	}

	public byte getTag() {
		return tag;
	}

	public void setTag(byte tag) {
		this.tag = tag;
	}

	public String toString() {
		return "x = " + x + ", y = " + y + ", z = " + z;
	}
}
