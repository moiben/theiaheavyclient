package theia.bpe.heavyclient;

import java.util.List;

public class Cloud2_old {
	private List<Vertex[]> frames;
	private boolean isLocked = false;

	public List<Vertex[]> getFrames() {
		return frames;
	}

	public void setFrames(List<Vertex[]> frames) {
		this.frames = frames;
	}

	public synchronized void lock() throws InterruptedException {
		while (isLocked) {
			wait();
		}
		System.out.println("now locked");
		isLocked = true;
	}

	public synchronized void unlock() {
		isLocked = false;
		notify();
	}
}
