package theia.bpe.heavyclient;

/**
 * Repr�sente un squelette fournit par le KinectStreamer (ensemble de paires
 * Cl�-Point).
 * 
 * @author Benjamin Peyresaubes
 *
 */
public class Body {
	private Vertex[] joints;
	private byte tag;
	private long timestamp;

	public Body() {

	}

	public Body(Vertex[] joints, byte tag, long timestamp) {
		this.joints = joints;
		this.tag = tag;
		this.timestamp = timestamp;
	}

	public synchronized Vertex[] getJoints() {
		return joints;
	}

	public synchronized void setJoints(Vertex[] joints) {
		this.joints = joints;
	}

	public byte getTag() {
		return tag;
	}

	public void setTag(byte tag) {
		this.tag = tag;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
