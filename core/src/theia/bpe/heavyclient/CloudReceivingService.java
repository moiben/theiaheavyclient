package theia.bpe.heavyclient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class CloudReceivingService extends ReceivingService {
	private Cloud cloud;

	public CloudReceivingService(int port, int packetSize, Cloud cloud) throws SocketException {
		super(port, packetSize);
		this.cloud = cloud;
	}

	@Override
	public void run() {
		byte[] buffer = new byte[packetSize];

		long timeStamp = 0;
		boolean newFrame = false;
		Vertex[] completeFrame = null;
		while (true) {
			DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
			newFrame = false;
			try {
				dgSocket.receive(receivePacket);

				byte[] data = receivePacket.getData();
				int actualLength = receivePacket.getLength();
				// System.out.println("Cloud : " + actualLength);

				ByteBuffer byteBuffer = ByteBuffer.wrap(Arrays.copyOfRange(data, 0, 8));
				long trunkTimeStamp = byteBuffer.getLong();
				if (trunkTimeStamp < timeStamp) {
					continue;
				} else if (trunkTimeStamp > timeStamp) {
					timeStamp = trunkTimeStamp;
					newFrame = true;
					if (completeFrame != null) {
						cloud.getQueue().offer(completeFrame);
					}
				}

				if (data.length - 8 > 0) {
					Vertex[] newVertex = new Vertex[(data.length - 8) / 16];
					int vertexIndex = 0;
					int dataIndex = 8; // 0 to 7 = timestamp
					while (dataIndex < data.length && dataIndex < actualLength) {
						float x = ByteBuffer.wrap(Arrays.copyOfRange(data, dataIndex, dataIndex + 4))
								.order(ByteOrder.LITTLE_ENDIAN).getFloat();
						float y = ByteBuffer.wrap(Arrays.copyOfRange(data, dataIndex + 4, dataIndex + 8))
								.order(ByteOrder.LITTLE_ENDIAN).getFloat();
						float z = ByteBuffer.wrap(Arrays.copyOfRange(data, dataIndex + 8, dataIndex + 12))
								.order(ByteOrder.LITTLE_ENDIAN).getFloat();

						byte r = data[dataIndex + 12];
						byte g = data[dataIndex + 13];
						byte b = data[dataIndex + 14];

						newVertex[vertexIndex] = new Vertex(x, y, z, r, g, b, (byte) 0);

						vertexIndex++;
						dataIndex += 16;
					}
					if (newFrame) {
						completeFrame = newVertex;
					} else {
						Vertex[] result = Arrays.copyOf(completeFrame, completeFrame.length + newVertex.length);
						System.arraycopy(newVertex, 0, result, completeFrame.length, newVertex.length);
						completeFrame = result;
					}
					// if (newFrame) {
					// cloud.lock();
					// cloud.setFrames(new ArrayList<>());
					// cloud.getFrames().add(newVertex);
					// cloud.unlock();
					// } else {
					// // Vertex[] oldVertex = cloud.getFrame().getPoints();
					// // Vertex[] result = Arrays.copyOf(oldVertex,
					// // oldVertex.length + newVertex.length);
					// // System.arraycopy(newVertex, 0, result,
					// // oldVertex.length, newVertex.length);
					// cloud.lock();
					// cloud.getFrames().add(newVertex);
					// cloud.unlock();
					// }

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
