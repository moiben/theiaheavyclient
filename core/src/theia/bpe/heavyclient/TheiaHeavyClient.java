package theia.bpe.heavyclient;

import java.net.SocketException;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

public class TheiaHeavyClient extends ApplicationAdapter {
	private PerspectiveCamera cam;
	private ModelBatch modelBatch;
	private Environment environment;

	private Model singlePointInCloud;
	private Model singleJointInSkeleton;
	private Body skeleton;
	private Cloud cloud;

	@Override
	public void create() {

		cloud = new Cloud();
		skeleton = new Body();
		ReceivingService cloudService;
		try {
			cloudService = new CloudReceivingService(9876, 3000, cloud);
			ReceivingService bodyService = new SkeletonReceivingService(9877, 600, skeleton);

			new Thread(cloudService).start();
			new Thread(bodyService).start();
		} catch (SocketException e) {
			e.printStackTrace();
		}

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

		cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(0, 0, 0);
		cam.lookAt(0, 0, 1);
		cam.update();

		modelBatch = new ModelBatch();
		ModelBuilder modelBuilder = new ModelBuilder();
		singlePointInCloud = modelBuilder.createBox(0.01f, 0.01f, 0.01f,
				new Material(ColorAttribute.createDiffuse(Color.WHITE)), Usage.Position | Usage.Normal);
		singleJointInSkeleton = modelBuilder.createBox(0.1f, 0.1f, 0.1f,
				new Material(ColorAttribute.createDiffuse(Color.GREEN)), Usage.Position | Usage.Normal);
		// instance = new ModelInstance(model);
	}

	@Override
	public void render() {
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		modelBatch.begin(cam);

		// int i = 0;
		// List<Vertex[]> frame = null;
		// Vertex[] vertices = null;
		// try {
		// cloud.lock();
		// frame = cloud.getFrames();
		// if (frame != null && !frame.isEmpty()) {
		// for (Vertex[] vertices2 : frame) {
		// vertices = vertices2;
		// for (i = 0; i < vertices.length; i++) {
		//
		// Color pointColor = new Color(((float) vertices[i].getR()) / 255f,
		// ((float) vertices[i].getG()) / 255f, ((float) vertices[i].getB()) /
		// 255f, 1f);
		// ModelInstance newPoint = new ModelInstance(singlePointInCloud,
		// (vertices[i].getX()),
		// (vertices[i].getY()), (vertices[i].getZ()));
		// newPoint.materials.get(0).set(ColorAttribute.createDiffuse(pointColor));
		// modelBatch.render(newPoint);
		// }
		// }
		// System.out.println("ok " + frame.size());
		// }
		// cloud.unlock();
		// } catch (InterruptedException | NullPointerException e) {
		// // e.printStackTrace();
		// System.out.println("Erreur : " + frame.size() + " " + i + " " +
		// vertices.length);
		// }
		Vertex[] vertices = cloud.getQueue().poll();
		if (vertices != null) {
			for (int i = 0; i < vertices.length; i++) {
				Color pointColor = new Color(((float) vertices[i].getR()) / 255f, ((float) vertices[i].getG()) / 255f,
						((float) vertices[i].getB()) / 255f, 1f);
				ModelInstance newPoint = new ModelInstance(singlePointInCloud, (vertices[i].getX()),
						(vertices[i].getY()), (vertices[i].getZ()));
				newPoint.materials.get(0).set(ColorAttribute.createDiffuse(pointColor));
				modelBatch.render(newPoint);
			}
			if (cloud.getQueue().isEmpty()) {
				cloud.getQueue().offer(vertices);
			}
		}

		Vertex[] skull = skeleton.getJoints();
		if (skull != null) {
			for (int i = 0; i < skull.length; i++) {
				modelBatch.render(
						new ModelInstance(singleJointInSkeleton, skull[i].getX(), skull[i].getY(), skull[i].getZ()));
			}
		}

		modelBatch.end();
	}

	@Override
	public void dispose() {
		modelBatch.dispose();
		singlePointInCloud.dispose();
		singleJointInSkeleton.dispose();
	}
}
