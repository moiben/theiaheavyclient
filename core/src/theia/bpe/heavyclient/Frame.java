package theia.bpe.heavyclient;

public class Frame {
	private Vertex[] points;

	public Frame(Vertex[] points) {
		this.points = points;
	}

	public Vertex[] getPoints() {
		return points;
	}
}
